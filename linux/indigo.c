#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(WIN32)
#include <windows.h>
DWORD WINAPI run_load(LPVOID lpParameter);
#else
#include <sys/mman.h>
#include <sys/wait.h>
#include <unistd.h>
#endif

int sys_bineval(char *argv);

int main(int argc, char *argv[])
{
	if (argc < 2) {
		printf("Argument: <alphanumeric-encoded code>\n");
		exit(-1);
	}

	sys_bineval(argv[1]);

	exit(0);
}

int sys_bineval(char *argv)
{
	size_t len;

#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(WIN32)
	int pID;
	char *code;
#else
	int *addr;
	size_t page_size;
	pid_t pID;
#endif

	len = (size_t)strlen(argv);

#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(WIN32)
	// allocate a +RWX memory page
	code = (char *) VirtualAlloc(NULL, len+1, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	// copy the code
	strncpy(code, argv, len);

	// execute it by ASM defined in run_load function
	WaitForSingleObject(CreateThread(NULL, 0, run_load, code, 0, &pID), INFINITE);
#else
	pID = fork();
	if(pID<0)
		return 1;

	if(pID==0)
	{
		page_size = (size_t)sysconf(_SC_PAGESIZE)-1;	// get page size
		page_size = (len+page_size) & ~(page_size);	// align to page boundary

		// mmap an +RWX memory page
		addr = mmap(0, page_size, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_SHARED|MAP_ANON, 0, 0);

		if (addr == MAP_FAILED)
			return 1;

		// copy the code
		strncpy((char *)addr, argv, len);

		// execute it
		((void (*)(void))addr)();
	}

	if(pID>0)
		waitpid(pID, 0, WNOHANG);
#endif

	return 0;
}

#if defined(_WIN64)
void __run_load(LPVOID);

DWORD WINAPI run_load(LPVOID lpParameter)
{
	__try
	{
		__run_load(lpParameter);
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
	}

	return 0;
}
#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
DWORD WINAPI run_load(LPVOID lpParameter)
{
	__try
	{
		__asm
		{
			mov eax, [lpParameter]
			call eax
		}
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
	}

	return 0;
}
#endif
